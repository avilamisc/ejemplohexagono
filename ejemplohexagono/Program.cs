﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hexagono
{
    class Program
    {
        static void Main(string[] args)
        {
            int intN = 0;
            int intInput = 0;
            string strC = "";
            int intCqty = 0;
            string strSpaces = " ";

            Console.WriteLine("Ingrese la cantidad de caracteres que debe tener cada lado del Hexagono:");
            intN = int.Parse(Console.ReadLine());
            intCqty = intN;
            intInput = intN;

            Console.WriteLine("Ingrese el caracter que representara el Hexagono:");
            strC = Console.ReadLine();

            Console.WriteLine("\n\n");

            while (intN >= 1)
            {
                Console.WriteLine(String.Concat(Enumerable.Repeat(strSpaces, (intN - 1))) + String.Concat(Enumerable.Repeat(strC, intCqty)));
                intCqty += 2;
                intN--;
            }
            intCqty -= 2;
            while (intN < (intInput - 1))
            {
                Console.WriteLine(String.Concat(Enumerable.Repeat(strSpaces, (intN + 1))) + String.Concat(Enumerable.Repeat(strC, intCqty - 2)));
                intCqty -= 2;
                intN++;
            }

            Console.ReadLine();
        }
    }
}
